# 1d-CFT-blocks-toolkit

This is a collection of Mathematica functions that can be used to compute relevant quantities for conformal blocks in one dimension such as Casimir equations and general terms of series expansions. These functions were also presented as supplemental material of the paper [2310.08632](https://arxiv.org/abs/2310.08632 "2310.08632") released in October 2023.

## List of features
With the functions presented in `1d_Blocks_Notebook.nb`, the user can perform a wide variety of operations regarding one-dimensional conformal blocks just by starting from an OPE diagram, represented as a Graph. These operations include:
- draw the associated conventional "flow diagram", that automatically encodes conventions for cross-ratios and prefactors
- automatically display the prefactors and cross-ratios associated with a flow diagram
- provide a map from physical space coordinates to cross-ratios via a conventional gauge fixing 
- work out the Casimir differential operators given the set of points involved in its definition
- compute the one-dimensional conformal blocks associated with a flow diagram via a set of Feynman-like rules